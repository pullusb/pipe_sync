import bpy
from bpy.types import Operator

from .ftp_func import *
from .path_func import *
from pathlib import Path
from datetime import datetime, timezone


def get_addons_prefs_and_set():
    '''
    Same as get_addon_prefs()
    but set specific value before returning
    '''
    import os
    addon_name = os.path.splitext(__name__)[0]
    preferences = bpy.context.preferences
    addon_prefs = preferences.addons[addon_name].preferences
    if not addon_prefs.admin_mode:
        addon_prefs.sync_type = "ONLY_NEW"
    return (addon_prefs)

## Globals
EXCLUSIONS = ['.*', '*.db', '*.blend?', '*~', '*sync-conflict*', '*.DS_Store']
# ROOT_FOLDERS = ['shots', 'library']

# task sequence shot version
# re_file = re.compile(r'(?P<task>[a-zA-Z]+)_s(?P<sequence>\d{2})_p(?P<shot>\d{2})_v(?P<version>\d{3})\.blend$', re.IGNORECASE)
# re_shotid = re.compile(r'(s\d{2}_p\d{2}).*.blend$', re.IGNORECASE)

def utc_to_local(utc_dt):
    return utc_dt.replace(tzinfo=timezone.utc).astimezone(tz=None)

def check_file(ftp, ftp_folder, fp, dst_file, sync_type):
    '''
    args: ftp obj, ftp folder (root path), local_filepath, dest path on ftp, sync type
    check if given filepath is OK to push with rules
    Return None if ok, error string if not
    '''
    
    ftp.cwd(ftp_folder)
    f = basename(fp)

    if sync_type == 'MORE_RECENT':
        ## rounded time in sec -> datetime obj
        src_time = datetime.utcfromtimestamp(round(os.path.getctime(fp)) )#convert to UTC
        # src_time = datetime.fromtimestamp(round(os.path.getctime(fp)) )

    if sync_type in ('ONLY_NEW', 'MORE_RECENT'):
        found = exist_ftp_file(ftp, dst_file)
        # print('found: ', found)
        if found:#found is either True or modification time in second
            # print ('exists')
            if sync_type == 'ONLY_NEW':
                return f'- exists - skip: {f}'
            
            elif sync_type == 'MORE_RECENT':
                if isinstance(found, bool):
                    return f'! no time to compare - skip: {f}'
                
                # modif time to compare (add 10 seconds for safety)
                found_time = datetime.strptime(found, r'%Y%m%d%H%M%S')
                # found_time = utc_to_local(found_time)#convert ftp universal time to local time
                # https://stackoverflow.com/questions/3694487/in-python-how-do-you-convert-seconds-since-epoch-to-a-datetime-object

                print('src: ', src_time)
                print('dst: ', found_time)
                delta = src_time - found_time
                print(f'delta: {delta} -> seconds: {delta.total_seconds()}')
                ## reconvert to timestamp
                # print('delta second: ', datetime.timestamp(src_time) - datetime.timestamp(found_time) )
                if src_time < found_time:
                    #source is older so skip
                    return f'- source older than dest - skip: {f}'


#### --------------- 
###  Single file IO sync 
#### ---------------

def show_message_box(message = "", title = "Message Box", icon = 'INFO'):
    mess = message
    def draw(self, context):
        if isinstance(mess, list):
            col = self.layout.column()
            for line in mess:
                if line.startswith(('!', '-')):
                    icon = 'CANCEL'
                    line = line.lstrip('!- ')
                elif line.startswith('/!\\ '):
                    icon = 'ERROR'
                    line = line.replace('/!\\','').lstrip()
                else:
                    icon = 'CHECKMARK'# PLUS
                col.label(text=line, icon=icon)
        else:
            self.layout.label(text=mess)
    
    bpy.context.window_manager.popup_menu(draw, title = title, icon = icon)

def ftp_push(self, context, ftp, ftp_folder, fp, dst_file, sync_type):
    '''
    Push file in ftp
    args: self for report, context
    ftp: ftp connec obj, ftp_folder: root path,
    fp: local file path, dst_file: dest path on ftp, sync type
    '''
    ## Construct relative filepath from project root
    err = check_file(ftp, ftp_folder, fp, dst_file, sync_type)
    if err:
        return err
        # self.report({'ERROR'}, err)
        # return {'CANCELLED'}

    print(f'- new {dst_file}')
    if context.scene.ftps_dryrun:## test mode, skip
        return #{'FINISHED'}
    
    ftp.cwd(ftp_folder)#go back to initial folder for operations
    createDirs(ftp, dirname(dst_file))

    try :
        with open(fp, 'rb') as fd:
            ftp.storbinary('STOR ' + dst_file, fd)

        # self.report({'INFO'}, f'transferred : {dst_file}')
        return 
    except :
        return f'Impossible to copy {fp}'

class BFTP_OT_send_current_file_to_ftp(Operator):
    """Send To FTP"""
    bl_idname = "ftpsync.send_current"
    bl_label = "Send Current To Ftp"
    bl_options = {'REGISTER'}

    def execute(self, context):
        prefs = get_addons_prefs_and_set()
        if check_credentials(self, context):
            return {'CANCELLED'}
        # error = check_credentials(prefs)
        # if error: self.report({'ERROR'})
        local_folder = clean_path(prefs.local_folder)
        ftp_folder = clean_path(prefs.ftp_folder)
        if not ftp_folder.startswith('/'):
            ftp_folder = '/' + ftp_folder
        
        fp = clean_path(bpy.data.filepath)
        # if prefs.local_folder and clean_path(prefs.local_folder) not in clean_path(fp)

        """ ## only shot mode (but can be an asset...)
        if '/shots/' in fp:
            reg_name = check_valid_filename()
            if not reg_name:
                sid = get_shot_id()
                if not sid():
                    self.report({'ERROR'}, 'File name has not the right pattern')
                    return {'CANCELLED'}
                # fallback to operator "push random shit to shot directory"...^^
                ## bpy.ops.ftpsync.push_to_shotdir
                return {'FINISHED'} """

        # ftp_path = fp[len(prefs.local_folder.rstrip('\\/')) + 1:]#slice()

        ftp = connect_to_ftp(prefs.domain, prefs.login, prefs.password, prefs.passive_connection)
        report_list = []
        
        # push main file
        dst_file = re.sub(local_folder, '', fp).strip('/')
        print('dst_file: ', dst_file)
        # err = ftp_push(self, context, ftp, ftp_folder, fp, dst_file, prefs.sync_type)#use pref sync type
        err = ftp_push(self, context, ftp, ftp_folder, fp, dst_file, 'MORE_RECENT')# force push newer
        if err:
            self.report({'ERROR'}, err)
            return {'CANCELLED'}#main file, so abort without checking dependencies
        report_list.append(f'Transferred: {dst_file}')

        # push dependencies
        for lib, libabs in zip(bpy.utils.blend_paths(absolute=False, packed=False, local=True), bpy.utils.blend_paths(absolute=True, packed=False, local=True) ):
            lfp = Path(libabs)
            if not lfp.exists():
                report_list.append(f'!! Not found:{lib}')
                continue
            if not lib.startswith('//'):
                report_list.append(f'/!\\ absolute:{lib}')
            if not lfp.as_posix().startswith(local_folder):
                report_list.append(f'!! out of project:{lib}')
                continue
            
            # trypush
            dst_file = re.sub(local_folder, '', lfp.as_posix() ).strip('/')

            ## sync type available (prefs.sync_type to use userpref) MORE_RECENT, ONLY_NEW, OVERWRITE
            err = ftp_push(self, context, ftp, ftp_folder, lfp.as_posix(), dst_file, 'ONLY_NEW')#prefs.sync_type
            if err:
                report_list.append(err)
            else:
                report_list.append(f'Transferred: {dst_file}')
        ftp.close()

        if not any(x.startswith('Transfer') for x in report_list):
            self.report({'ERROR'}, '\n'.join(report_list))
        else:
            show_message_box(message=report_list, title = "Transfer report")
        
        print('--Transfer report--')
        for item in report_list:
            print(item)

        return {'FINISHED'}


class BFTP_OT_get_shot_from_ftp(Operator,):
    '''Pull specific files from FTP'''
    bl_idname = "ftpsync.get_shot"
    bl_label = "Get Chosen Shot From Ftp"
    bl_options = {'REGISTER'}#, 'UNDO'

    mysid : bpy.props.StringProperty(name="shot id", default="")# ,options={'SKIP_SAVE'}
    # mytask : #maybe choose a task enum can be dynamically load from yml template ?

    def invoke(self, context, event):
        prefs = get_addons_prefs_and_set()
        if not prefs.local_folder:
            self.report({'ERROR'}, f'Project local folder is not specified in addon preferences')
            return {'CANCELLED'}

        ## root path error handling
        self.root = clean_path(prefs.local_folder)
        err = check_root_path(self.root)
        if err:
            self.report({'ERROR'}, err)
            return {'CANCELLED'}
        
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout
        layout.label(text='shot ID ? (ex: "s02p01" ou "02 01"')
        layout.prop(self, 'mysid')#, text="")#, text=""

    def execute(self, context):
        sid = re.search(r'(\d{2}).*?(\d{2})', self.mysid, re.IGNORECASE)
        if not sid:
            self.report({'ERROR'}, 'No correct shot ID found')
            return {'CANCELLED'}
        
        shotfolder = f"S{sid.group(1)}_P{sid.group(2)}"        
        # construct filepath

        prefs = get_addons_prefs_and_set()
        if check_credentials(self, context):
            self.report({'ERROR'}, 'Problem with credentials')
            return {'CANCELLED'}
        ftp = connect_to_ftp(prefs.domain, prefs.login, prefs.password, prefs.passive_connection)

        if not prefs.local_folder :
            self.report({'ERROR'}, 'You have to set the project folder in your prefs')
            return {'CANCELLED'}

        print('\nStart Downloading from FTP')
        print('Local project Folder :', prefs.local_folder)

        ## normalize ftp path and local path
        local_folder = clean_path(prefs.local_folder)
        ftp_shot_path = f'/shots/{shotfolder}'#clean_path(prefs.ftp_folder)
        # if not ftp_folder.startswith('/') : ftp_folder = '/' + ftp_folder

        ## ensure it's at the root of the project then concatenate with shot/shotnum
        proj_shot_path = Path(local_folder.split('shots')[0]) / ftp_shot_path.strip('/ ')
        # NOTE: strip left '/' super important to avoid going back to root

        print('proj_shot_path: ', proj_shot_path.as_posix())


        ## change explore function according to preference
        if prefs.last_version:
            getfiles = get_last_ftp_files
        else:
            getfiles = get_ftp_files

        for f in getfiles(ftp, ftp_shot_path, EXCLUSIONS) :
            # print('src_file: ', f)
            # dst_file = Path(local_folder) / f.lstrip('/ ')# local folder can be wrong...
            dst_file = proj_shot_path / re.sub(ftp_shot_path, '', f.replace('\\','/')).strip('/')
            # print('dst_file: ', dst_file)


            # if prefs.sync_type == 'ONLY_NEW' and exists(dst_file) :
            if dst_file.exists() :# ALWAYS SKIP ON EXISTS WHEN PULLING
                continue

            print(f'- new {dst_file}')
            if context.scene.ftps_dryrun:
                continue

            if not dst_file.parent.exists() :
                dst_file.parent.mkdir(parents=True, exist_ok=True)# mode=0o777
            # if not exists(dirname(dst_file)) :
            #     os.makedirs(dirname(dst_file))

            with open(dst_file, 'wb') as fp:
                ftp.retrbinary('RETR ' + f, fp.write)

            #print('New file :', dst_file)


        ftp.close()
        open_folder(proj_shot_path)
        return {'FINISHED'}


#### --------------- 
###  ALL file IO sync 
#### ---------------

class BFTP_OT_send_to_ftp(Operator):
    """Send To FTP"""
    bl_idname = "ftpsync.send"
    bl_label = "Send All To Ftp"
    bl_options = {'REGISTER'}

    last_versions : bpy.props.BoolProperty(default=True)

    def execute(self, context):

        prefs = get_addons_prefs_and_set()
        if check_credentials(self, context):
            return {'CANCELLED'}
        ftp = connect_to_ftp(prefs.domain, prefs.login, prefs.password, prefs.passive_connection)

        local_folder = clean_path(prefs.local_folder)
        local_path = Path(local_folder)

        ftp_folder = clean_path(prefs.ftp_folder)
        if not ftp_folder.startswith('/'):
            ftp_folder = '/' + ftp_folder

        print('\nStart Uploading to the FTP')
        print('Source Folder :', prefs.local_folder)
        
        exclude_patterns = EXCLUSIONS + [i.pattern for i in prefs.exclude_filter]

        if self.last_versions:
            filelist = get_last_files(local_folder, exclude_patterns)
            print('Last file only mode')
        else:
            filelist = get_files(local_folder, exclude_patterns)
            print('All files mode')

        subpath = [i.path for i in prefs.include_filter]
        if subpath:
            #full path local projet folder / subpath join
            dir_to_sync = tuple([(local_path / d).as_posix() for d in subpath]) #old : ROOT_FOLDERS
        else:
            dir_to_sync = tuple(local_path.as_posix()) #old : ROOT_FOLDERS
        print('Dir_to_sync: ', dir_to_sync)
        
        filelist = [f.replace('\\','/') for f in filelist] # or Path(f).as_posix()
        root_sync = ftp_folder == '/'# if sync from root

        for f in filelist:
            if root_sync:
                if not f.startswith(dir_to_sync):
                    continue

            ## recursively get files in directory if not in exclusion list
            ftp.cwd(ftp_folder)
            dst_file = re.sub(local_folder, '', f).strip('/')

            err = check_file(ftp, ftp_folder, f, dst_file, prefs.sync_type)
            if err:
                print(f, err)
                continue
            
            print(f'- new {dst_file}')
            if context.scene.ftps_dryrun:
                continue

            ftp.cwd(ftp_folder)
            createDirs(ftp,dirname(dst_file))

            try :
                with open(f, 'rb') as fd:
                    ftp.storbinary('STOR ' + dst_file, fd)

                # print('New file :', dst_file)
            except :
                print(f'Impossible to copy {fd}')
        ftp.close()

        return {'FINISHED'}


class BFTP_OT_get_from_ftp(Operator,):
    '''Pull files from FTP'''
    bl_idname = "ftpsync.get"
    bl_label = "Get All From Ftp"
    bl_options = {'REGISTER'}#, 'UNDO'


    def execute(self, context):
        prefs = get_addons_prefs_and_set()
        if check_credentials(self, context):
            return {'CANCELLED'}
        ftp = connect_to_ftp(prefs.domain, prefs.login, prefs.password, prefs.passive_connection)

        if not prefs.local_folder :
            self.report({'ERROR'}, 'You have to set the local folder in your prefs')
            return {'CANCELLED'}

        print('\nStart Downloading from FTP')
        print('Local project Folder :', prefs.local_folder)

        
        ## normalize ftp path and local path
        local_folder = clean_path(prefs.local_folder)
        ftp_folder = clean_path(prefs.ftp_folder)
        subpathlist = [clean_path(i.path) for i in prefs.include_filter]

        if not ftp_folder.startswith('/') : ftp_folder = '/' + ftp_folder
            
        ## list of file to sync:
        if subpathlist:
            targets = [(Path(ftp_folder) / p).as_posix() for p in subpathlist]
        else:
            # just use FTP root folder
            targets = [ftp_folder]
        
        print('targets: ', targets)
        
        # Change method if "last version" option in pref is On
        if prefs.last_version:
            print('Last file only')
            getfiles = get_last_ftp_files
        else:
            print('All file')
            getfiles = get_ftp_files

        exclude_patterns = EXCLUSIONS + [i.pattern for i in prefs.exclude_filter]
        print('Exclude patterns: ', exclude_patterns)

        for tgt_folder in targets:
            print(f'--> {tgt_folder}')
            for f in getfiles(ftp, tgt_folder, exclude_patterns):
                # print('f: ', f)

                ## delete head of ftp proejct path (witch sub) and attach local project folder
                dst_file = join(local_folder, re.sub(ftp_folder, '', f.replace('\\','/')).strip('/'))

                # if prefs.sync_type == 'ONLY_NEW' and exists(dst_file) :
                if exists(dst_file) :# ALWAYS SKIP ON EXISTS WHEN PULLING
                    continue

                print(f'- new {dst_file}')
                if context.scene.ftps_dryrun:
                    continue

                if not exists(dirname(dst_file)) :
                    os.makedirs(dirname(dst_file))

                with open(dst_file, 'wb') as fp:
                    ftp.retrbinary('RETR ' + f, fp.write)

                # print('New file :', dst_file)

        ftp.close()
        
        self.report({'INFO'}, 'Done')
        return {'FINISHED'}


class BFTP_OT_get_dependencies_from_ftp(Operator,):
    '''Pull dependencies files from FTP'''
    bl_idname = "ftpsync.get_dependencies"
    bl_label = "Check And Get Dependencies"
    bl_options = {'REGISTER'}#, 'UNDO'

    def invoke(self, context, event):
        prefs = get_addons_prefs_and_set()
        if not prefs.local_folder :
            self.report({'ERROR'}, 'You have to set the local folder in your prefs')
            return {'CANCELLED'}

        ## normalize ftp path and local path
        local_folder = clean_path(prefs.local_folder)
        # ftp_folder = clean_path(prefs.ftp_folder)

        ## check dependencies
        self.broken = []
        self.abs_libs = []
        self.warning = []
        self.out_of_pipe = []
        for current, lib in zip(bpy.utils.blend_paths(local=True), bpy.utils.blend_paths(absolute=True, local=True)):
            lfp = Path(lib)
            if not current.startswith('//'):
                abs_broken = '' if lfp.exists() else '(broken)'
                self.warning.append(f'Not relative path{abs_broken}: {current}')
                ## maybe try to find it as relative if abspath contain project...
                continue
            
            # realib = Path(current)
            if not lfp.exists():
                if not clean_path(lib).startswith(local_folder):
                    # check if in pipe
                    self.out_of_pipe.append(f'Out of pipe: {current}')
                else:
                    self.broken.append(current)
                    self.abs_libs.append(lfp)

        print('-- Dependencies Check--')
        for o in self.out_of_pipe:
            print(o)
        for w in self.warning:
            print(w)
        for b in self.broken:
            print(f'Broken: {b}')
        
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width = 600)

    def draw(self, context):
        layout = self.layout
        if self.warning:
            # layout.label(text=warning)
            for i in self.warning:
                layout.label(text=i)
        if self.out_of_pipe:
            # layout.label(text=out_of_pipe)
            for i in self.out_of_pipe:
                layout.label(text=i)
        if self.broken:
            # layout.label(text=broken)
            for i in self.broken:
                layout.label(text=f'Broken: {i}')

            layout.label(text='-- Download from FTP ? --')

        else:
            layout.label(text='-- Nothing to fix --')
        

    def execute(self, context):
        if not self.broken:
            print('Nothing to fix')
            return {'FINISHED'}

        prefs = get_addons_prefs_and_set()
        local_folder = clean_path(prefs.local_folder)
        ftp_folder = clean_path(prefs.ftp_folder)


        if check_credentials(self, context):
            return {'CANCELLED'}
        ftp = connect_to_ftp(prefs.domain, prefs.login, prefs.password, prefs.passive_connection)

        print('\nStart Checking on FTP')
        print('Local project Folder :', prefs.local_folder)

        if not ftp_folder.startswith('/') : ftp_folder = '/' + ftp_folder

        reports = []
        ct = 0
        
        for f in self.abs_libs:
            dst_file = f.as_posix()
            # ftp_path = re.sub(local_folder, '', dst_file).strip('/')
            ftp_path = re.sub(local_folder, ftp_folder, dst_file).strip('/')
            ftp_path = '/' + ftp_path
            print('ftp_path: ', ftp_path)
            ftp.cwd(ftp_folder)
            found = exist_ftp_file(ftp, ftp_path)
            if not found:
                reports.append(f'Not found on FTP: {ftp_path}')
                continue

            # if prefs.sync_type == 'ONLY_NEW' and exists(dst_file) :
            if f.exists() :# ALWAYS SKIP ON EXISTS WHEN PULLING
                continue

            print(f'- new {dst_file}')
            ct += 1
            if context.scene.ftps_dryrun:
                continue

            if not exists(dirname(dst_file)) :
                os.makedirs(dirname(dst_file))

            with open(dst_file, 'wb') as fp:
                ftp.retrbinary('RETR ' + ftp_path, fp.write)

            reports.append(f'Downloaded: {f}')
            # print('New file :', dst_file)

        ftp.close()
        
        print('Report')
        if reports:
            for i in reports:
                print('-', i)

        self.report({'INFO'}, f'{ct}/{len(self.broken)} downloaded (see console)')
        return {'FINISHED'}


### register ---


cls = (
BFTP_OT_send_current_file_to_ftp,
BFTP_OT_get_shot_from_ftp,
BFTP_OT_send_to_ftp,
BFTP_OT_get_from_ftp,
BFTP_OT_get_dependencies_from_ftp,
)

def register():
    for c in cls :
        bpy.utils.register_class(c)

def unregister():
    for c in cls :
        bpy.utils.unregister_class(c)
