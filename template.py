# version 1.0.0 2020-07-02
 
import os
from os.path import *
import re
import yaml
import json
from glob import glob
from pathlib import Path


ALL = '[?]'
NONE = '[?]'
FIRST = '[0]'
PREVIOUS = '[x-1]'
NEXT = '[x+1]'
LAST = '[-1]'
LAST_NEXT = '[x+1][-1]'

FILTERS = [ALL, NONE, FIRST, PREVIOUS, NEXT, LAST, LAST_NEXT]


class Field :
    type = str
    glob_str = '*'
    reg_pattern = r'([\w:\/\-" +"]+)'

    def sub_reg(self, pattern):
        return pattern.replace(self.raw, self.reg_pattern, 1)

    def sub_string(self, pattern):
        return pattern

    def sub_glob(self, pattern):
        return pattern.replace(self.raw, self.glob_str, 1)

class FieldEnv(Field):
    def __init__(self, name):
        self.name = name
        self.raw = '{%s}'%name

    def sub_string(self, pattern):
        return pattern.replace('{$%s}'%self.name, self.raw, 1)

    def sub_glob(self, pattern):
        return pattern.replace('{%s}'%self.name,
        os.getenv(self.name, self.glob_str), 1)

    @classmethod
    def from_str(cls, string) :
        result = re.findall(r'{\$(\w+)}', string)
        if result :
            return cls(result[0])

    def sub_reg(self, pattern):
        return pattern.replace('{$%s}'%self.name, self.reg_pattern, 1)

    def format(self, value) :
        return value

    def _norm_args(self, key, value, data, types=[]) :
        if value in (ALL, LAST_NEXT, None) :
            return

        return value

class FieldStr(Field):
    def __init__(self, name):
        self.name = name
        self.raw = '{%s}'%name

    @classmethod
    def from_str(cls, string) :
        result = re.findall(r'{(\w+)}', string)
        if result :
            return cls(result[0])

    def format(self, value) :
        return value

    def _norm_args(self, key, value, data, types=[]) :
        if value in (ALL, LAST_NEXT, None) :
            return

        if (value in FILTERS and data[key] not in types) :
            raise Exception(f'The {key} "{data[key]}" is not in list {types}')
        elif (types and data[key] not in types) :
            raise Exception(f'The {key} "{value}" is not in list {types}')
        elif not types :
            return value


        index = types.index(data[key])
        if value == FIRST :
            return types[0]
        elif value == PREVIOUS:
            return types[max(0, index-1)]
        elif value == NEXT :
            return types[min(len(types)-1, index+1)]
        if value == LAST :
            return types[-1]

        return value


class FieldInt(Field):
    def __init__(self, name, padding=1):
        self.type = int
        self.name = name
        self.raw = '{%s:%02dd}'%(name, padding)
        self.glob_str = '?'*padding
        self.reg_pattern = r'(\d{%d})'%padding

    @classmethod
    def from_str(cls, string) :
        result = re.findall(r'{(\w+):(\d{2}d)}', string)
        if result :
            name, padding_str = result[0]
            padding = len(format(0, padding_str) )
            return cls(name, padding)

    def format(self, value) :
        return int(value)

    def _norm_args(self, key, value, data, types=[]) :
        if value==LAST :
            return
        elif value == FIRST :
            return 0
        elif value == PREVIOUS:
            return  max(0, data[key] -1)
        elif value == NEXT :
            return data[key] +1

        return value


class Template :
    def __init__(self, string, types={}, fields=[]):
        reg_field = re.compile(r'{[\w:\$]+}')

        self.types = types
        self.field_types = [FieldEnv, FieldStr, FieldInt] + fields
        self.string = string
        self.reg = string.replace('/','\/')

        self.fields = []
        for match in reg_field.findall(self.reg) :
            for field_class in self.field_types :
                field = field_class.from_str(match)
                if field :
                    self.fields.append(field)
                    self.reg = field.sub_reg(self.reg)
                    self.string = field.sub_string(self.string)

        self.reg = re.compile(self.reg)

    def _norm_args(self, data, **kargs) :
        missing_fields = set( {**data, **kargs} )-set( [f.name for f in self.fields] )
        if missing_fields:
            print(f"Fields : {missing_fields} are not in the pattern : {self.string}")

        data = data.copy()
        args = kargs.copy()

        for key, value in kargs.items() :
            field = next((f for f in self.fields if f.name == key), None)

            types_value = self.types.get(key, [])
            args[key] = field._norm_args(key, value, data, types_value)

        return {**data, **args}

    def _filter_data(self, datas, filters, filtered_data) :
        if not filters :
            filtered_data.extend(datas)
            return filtered_data

        key, filter, value = list(filters)[0] # Filter one by one

        values = [d[key] for d in datas]

        if filter in (PREVIOUS, NEXT) :
            values.append(value)

        values = sorted(list( set(values) ))
        if key in self.types :
            values.sort(key=lambda x : self.types[key].index(x) )
        #    values = self.types[key]
        #else :
        #    values = sorted([d[key] for d in datas])
            #values.sort(key=lambda x : self.types[key].index(x) )

        #print('values', values)
        #print('datas', datas)
        #print('filters', filters)
        #print('key, filter, value', key, filter, value)

        if filter == ALL :
            for v in set([d[key] for d in datas]) :
                new_datas = [d for d in datas if d[key]==v]
                self._filter_data(new_datas, filters[1:], filtered_data)

        elif filter not in FILTERS :
            new_datas = [d for d in datas if d[key] == filter]
        elif filter == LAST :
            new_datas = [d for d in datas if d[key] == values[-1] ]
        elif filter == FIRST :
            new_datas = [d for d in datas if d[key] == values[0]]
        elif filter == PREVIOUS :
            if not value :
                raise(f'The key {key} has to be in the data')
            index = values.index(value)
            filter_value = None if index == 0 else values[index-1]
            new_datas = [d for d in datas if d[key] == filter_value]
        elif filter == NEXT :
            if not value :
                raise(f'The key {key} has to be in the data')
            index = values.index(value)
            filter_value = None if index == len(values)-1 else values[index+1]
            new_datas = [d for d in datas if d[key] == filter_value]

        elif filter == LAST_NEXT :
            new_datas = [{**d,**{key: d[key]+1}} for d in datas if d[key] == values[-1]]

        return self._filter_data(new_datas, filters[1:], filtered_data)

    def parse(self, string, strict_parsing=True) :
        if isinstance(string, dict) :
            return string

        string = Path(string).as_posix()

        data = {}
        result = self.reg.findall(string)


        if not result :
            print(f'Could not parse : {string} with pattern : {self.reg}')
            return

        if isinstance(result[0], tuple) :
            result = result[0]

        if len(result) != len(self.fields) :
            print(f'The number of fields mismatch : {string}, pattern : {self.reg}, fields : {self.fields}')
            return

        for field, field_value in zip(self.fields, result) :
            field_value = field.format(field_value)

            if field.name in data :
                other_value = data[field.name]
                if other_value != field_value and strict_parsing :
                    raise Exception(f'The field "{field.name}" '+
                    f'as different values : {field_value}, {other_value}')
            else :
                data[field.name] = field_value

        return data

    def format(self, data, **kargs) :
        if isinstance(data, str) :
            data = self.parse(data)

        data = self._norm_args(data, **kargs)
        glob_pattern = self.string
        for field in self.fields :
            if field.name not in data or data[field.name] is None :
                glob_pattern = field.sub_glob(glob_pattern)

        #print(glob_pattern, data)
        return normpath(glob_pattern.format(**data) )

    def find_all(self, data, get=None, **kargs) :
        data = self.parse(data)
        norm_args = {k:None if v in FILTERS else v for k,v in kargs.items()} # replace FILTERS args by ALL

        #print('##data', data)
        #print('##norm_args', norm_args)

        pattern = self.format(data, **norm_args)
        #print('pattern\n', pattern)


        paths = [f.replace('\\','/') for f in sorted( glob(pattern) ) ]

        #print('1 Paths\n', paths)

        if not paths : []

        datas = [self.parse(p) for p in paths]

        ## Transform filter argument by value if has type
        filters = [(k, v, data.get(k)) for k, v in kargs.items()]

        #print("paths", paths)
        #print("datas", datas)

        datas = self._filter_data(datas, filters, [])

        if get :
            out = [d[get] for d in datas]
        else :
            out = [self.format(d) for d in datas]

        return sorted( list(set(out)) )

    def find(self, data, **kargs):
        result = self.find_all(data, **kargs)
        if result :
            return result[0]

    def find_dirs(self, data) :
        fields = {f.name: self.types[f.name] for f in
        self.fields if f.name in self.types}

        folders = []
        for field_name, field_values in fields.items() :
            other_fields = {k:v for k,v in fields.items() if k!=field_name}
            for field_value in field_values :
                if other_fields :
                    for name, values in other_fields.items() :
                        for v in values :
                            d = {**{field_name:field_value, name : v}, **data}
                            folders.append( self.format(d) )
                else :
                    d = {**{field_name:field_value},**data }
                    folders.append(self.format(d) )

        return list(set(folders))

    def make_dirs(self, data) :

        folders = self.find_dirs(data)

        #Create all possible parent folder for a shot_file
        for f in folders :
            Path(f).parent.mkdir(exist_ok=True, parents=True)

        return folders

    def __repr__(self) :
        return f'\nTemplate(\n{v.string}\n)'

class Templates :
    def __init__(self, template=None, types=[], fields=[]) :
        if not template :
            template = os.getenv('TEMPLATES')

        if isinstance(template, str) :
            template = re.split('[;,]',template)

        if not isinstance(template, (list, tuple) ) :
            template = [template]

        if not isinstance(types, (list, tuple) ) :
            types = [types]

        self.strict_parsing = True

        #print('template', template)

        templates = []
        for template in [ self._read_template(t) for t in template ] :
            if isinstance(template, (list, tuple)) :
                template, type = template
                types.append(type)

            templates.append(template)

        self.types = self._merge_dict(types) # available value for field
        self.raw = self._merge_dict(templates) # available value for field
        self.expanded = self._expanded(self.raw)

        #print('expanded', self.expanded)
        #print('types', self.types)

        self.templates = {k : Template(v, self.types, fields) for k, v in self.expanded.items() }# Init Template class

    def _merge_dict(self, dicts) :
        data = {}
        for d in dicts :
            data.update(d)

        return data

    def _read_yml(self, path) :
        return list(yaml.safe_load_all(path.read_text() ) )

    def _read_template(self, template) :
        if isinstance(template, dict) :
            return template

        path = Path(template)
        #path = self.dir/f'{template}.yml'
        if not path.exists() :
            raise Exception(f'The template "{template}" does not exist')

        return self._read_yml(path)

    def _expanded(self, templates):
        reg_ref = re.compile(r'{\@(\w+)}')
        reg_list = re.compile(r'{(\w+):\@(\w+)}')
        reg_env = re.compile(r'{\$(\w+)}')

        expanded_templates = templates.copy()
        #print('expanded_templates', expanded_templates)

        for template_name, template in expanded_templates.items() :
            #print(template_name, template)
            ref_fields = reg_ref.findall(template)
            while ref_fields :
                for ref_field in ref_fields :
                    ref_value = expanded_templates.get(ref_field)
                    if not ref_value :
                        raise Exception(f'The key "{ref_fields}" is not in the template')
                    template = template.replace('{@%s}'%ref_field, ref_value)

                ref_fields = reg_ref.findall(template)

            '''
            for env_field in reg_env.findall(template):
                env_value = os.getenv(env_field)
                if not env_value :
                    raise Exception(f'The key "{env_field}" is not in the env')
                template = template.replace('{$%s}'%env_field, env_value)
            '''
            expanded_templates[template_name] = template

        return expanded_templates


    def parse(self, path, template_in=None):
        if isinstance(path, dict) :
            return path

        if not template_in :
            template_in = self.current

        if template_in not in self.keys() :
            raise Exception(f'{template_in} not in {list(self.keys())}')

        template = self[template_in]
        return template.parse(path, self.strict_parsing)


    def format(self, data, template_in=None, template_out=None, **kargs) :
        if not template_in :
            template_in = self.current

        if isinstance(data, str) :
            data = self.parse(data, template_in)

        template_out = template_out if template_out else template_in
        template = self[template_out]

        return template.format(data, **kargs)


    def find_all(self, data, template_in=None, template_out=None, **kargs):
        if not template_in :
            template_in = self.current

        if isinstance(data, str) :
            data = self.parse(data, template_in)

        template_out = template_out if template_out else template_in
        template = self[template_out]

        return template.find_all(data, **kargs)


    def find(self, data, template_in=None, template_out=None, **kargs):
        result = self.find_all(data, template_in, template_out, **kargs)
        if result :
            return result[0]


    def norm_types(self, data) :
        norm_data = data.copy()

        fields = {}
        for t in self.values() :
            for f in t.fields :
                fields[f.name] = f.type

        for key, value in data.items() :
            if key in fields :
                norm_data[key] = fields[key](value)

            if key not in self.types :
                continue
            v = self.norm_str(value)
            t = {self.norm_str(t) : t for t in self.types[key]}

            if v in t :
                norm_data[key] = t[v]

        return norm_data

    def norm_str(self, string) :
        import unicodedata
        string = string.lower().strip(' ')
        string = string.replace('_','-').replace(' ', '-')
        string = unicodedata.normalize('NFKD', string).encode('ASCII', 'ignore').decode("utf-8")

        return string

    def __repr__(self) :
        text = '\nTemplates(\n'
        for k, v in self.items() :
            text+= f"    {k} : {v.string}\n"
        text+= ')\n'

        return text

    def __getitem__(self, k):
        return self.templates[k]

    def items(self) :
        return self.templates.items()

    def keys(self) :
        return self.templates.keys()

    def values(self) :
        return self.templates.values()

    def __iter__(self):
        return (i for i in self.values() )


if __name__ == '__main__':
    #import doliprane as dlp
    #import os

    #env = dlp.Env('acs')
    #templates = dlp.Templates()

    #print(templates)
    #print(templates)
    #print(templates)

    root = "D:/projet_perso/prog/doliprane/doliprane/store/short-film/automated-customer-service"
    file = root+"/ACS/sequences/se000/sh0000/animatic/ACS_se000_sh0000_animatic_v000.blend"

    #blend = "ACS_se000_sh0000_animatic_v000.blend"

    #data = {}

    #path = templates.format(file, 'shot_file', version= dlp.LAST)
    #print('\n','path :', '\n',path,'\n')

    #data  = templates.format(file, 'shot_file',shot_task="test")
    #print('\n','data :', '\n',data,'\n')

    #new_path = templates.format(file, 'shot_file', 'task_dir')
    #print('\n','new_path :', '\n',new_path,'\n')


    # Find all version of a shot task
    #paths = templates.find_all(file, 'shot_file', sequence=dlp.ALL, shot=dlp.ALL, shot_task=dlp.FIRST,version=dlp.FIRST)
    #print('\nFind all version of a shot task')

    # Find first version of a shot ?
    new_path = templates.find(file, 'shot_file', version=FIRST, task='layout')
    print('\nFind first version of a shot task')
    print('Paths :', new_path,'\n')

    # Find previous task for a shot last version
    new_path = templates.find(file, 'shot_file', task=PREVIOUS, version=LAST)
    print('\nFind previous task for a shot last version')
    print('Path :', new_path,'\n')

    # Find next task for a shot version 1
    new_path = templates.find(file, 'shot_file', task=NEXT, version=1)
    print('\nFind next task for a shot version 1')
    print('Path :', new_path,'\n')

    # Find last last task last version of a shot
    new_path = templates.find(file, 'shot_file', task=LAST, version=LAST)
    print('\nFind last task last version of a shot')
    print('Path :', new_path,'\n')


    # Find last last task last version of all shots
    paths = templates.find_all(file, 'shot_file',
        sequence=ALL,
        shot=ALL,
        task=LAST,
        version=LAST)
    print('\nFind last last task last version of all shots')
    print('Paths :', paths,'\n')





    #paths = templates.find_first(file, 'shot_file', task=dlp.ALL)
    #print('\n','new_paths :', '\n',paths,'\n')
