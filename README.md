# Pipe sync

FTP sync from blender with specific folder structure + local tools

Structure exemple
(can be change in template.yml, some part of the code may still be hardcoded to match this template)

```
S01_P01
    layout
        layout_s01_p02_v001.blend

    anim
        anim_s01_p02_v001.blend
        images
            playblast_anim_s01_p02_v001.mp4

    ...
```


Structure

regex task, sequence, shot, version

`(?P<task>[a-zA-Z]+)_s(?P<sequence>\d{2})_p(?P<shot>\d{2})_v(?P<version>\d{3})\.blend$`


full sync:

- [x] push all

- [X] push all 'last'

- [x] retrieve all

- [x] retrieve all 'last'

focused sync:

- [X] credential load/save

- [X] push local (force "more recent" mode - push if the local file is )

    - [X] push local with dependancies (force "only new" mode - don't push if file exists)

- [X] retrieve local to hierarchy (external or popup to choose shot to sync within blender)

- [X] retrieve dependencies of current shots (pop up list before)


local manipulation:

- [X] increment to a save

- [X] create next task

<!-- - [ ] -->

# External tool

- [ ] launcher (external ?)

- [ ] retrieve wanted shot

- [ ] list only assigned task filter

## TODO

- Accepts empty folders, option for type of sync (override etc)


## Changelog


1.9.1:

- fix for API changes

1.9.0:

- feat: Check and retrieve file dependancies

1.8.1:

- Feat: Possible to download only last files when versionned
- Feat: add subpath filter, download only from these subpath
- Feat: add user defined exclusion pattern (added to default exclusion list `['.*', '*.db', '*.blend?', '*~', '*sync-conflict*', '*.DS_Store']`)
    - maybe this should changed to all pattern in one list of coma separated values.
- UI: change addon preference UI to reflect additions
    - add option `last only` in preference (True by default, only valid for "get all" for now)
    - mutli lines for sync folder filter and exclusion
- import-export: updated to include folderlist and exclude filters
- warn: Upload might be broken in this version (untested)

1.7.0:

- Fix: broken gitlab updater (used repo id instead of repo name)

1.6.5:

- Fix: bug when project is at root of a drive

1.6.4:

- force mode 'more recent' mode to push current file (more logical)

1.6.3:

- fix: Create path is not exists when saving get next task.

1.6.2:

- admin mode False by defaut (to hide full sync feature and force 'only new' mode for sync when disabled)
- fix: missing unregister
- note: gitlab updater still broken (error 404)

1.6.1:

- Added updater
- Switched to gilab

1.5.4:

- retrieve chosen shot

1.5.3:

- Updated [template.py](https://gitlab.com/ChristopheSeux/template) from Christophe Seux
- fix bug when project local folder url starts with slash
- added dependancy push (only_new mode)

1.5.2:

- push only last version
- fix: missing datetime import for push comparison

1.5.1:

- fix: Add error handling when parsing filepath return None
- add

1.5.0:

- refactor
