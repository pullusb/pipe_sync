import bpy
import json
from bpy_extras.io_utils import ImportHelper, ExportHelper
from bpy.types import (
    Operator,
    AddonPreferences,
    PropertyGroup,
)

from bpy.props import (
    IntProperty,
    StringProperty,
    BoolProperty,
    EnumProperty,
    CollectionProperty,
)

from . import addon_updater_ops  # upm
from .ftp_func import *
from .path_func import *


class IncludePath(PropertyGroup):
    # Real path of just keywords ???
    path: StringProperty(name="Path", subtype='FILE_PATH')

class ExclusionPatterns(PropertyGroup):
    # Real path of just keywords ???
    pattern: StringProperty(name="Pattern")


class PIPE_OT_add_filter_folder(Operator):
    bl_idname = "ftpsync.add_filter_folder"
    bl_label = 'Add a an element to a collection prop'

    name: StringProperty()

    def execute(self, context):
        prefs = bpy.context.preferences.addons[__package__].preferences
        ## generic object add...
        if not hasattr(prefs, self.name):
            print(f'could not find {self.name} prop in preferences')
            return {'CANCELLED'}
        # like prefs.include_filter.add()
        getattr(prefs, self.name).add()

        return {'FINISHED'}


class PIPE_OT_remove_filter_folder(Operator):
    bl_idname = "ftpsync.remove_filter_folder"
    bl_label = 'Remoce an element from a collection prop'

    index: IntProperty()
    name: StringProperty()

    def execute(self, context):
        prefs = bpy.context.preferences.addons[__package__].preferences
        # prefs.include_filter.remove(self.index)
        getattr(prefs, self.name).remove(self.index)

        return {'FINISHED'}


class PIPE_Preferences(AddonPreferences):
    bl_idname = __name__.split('.')[0]  # os.path.splitext(__name__)[0]

    pref_tabs: bpy.props.EnumProperty(
        items=(('PREF', "Preferences", "Change some preferences of the modal"),
               #    ('MAN_OPS', "Operator", "Operator to add Manually"),
               # ('TUTO', "Tutorial", "How to use the tool"),
               ('UPDATE', "Update", "Check and apply updates"),
               # ('KEYMAP', "Keymap", "customise the default keymap"),
               ),
        default='PREF')

    # addon pref updater props

    auto_check_update: BoolProperty(
        name="Auto-check for Update",
        description="If enabled, auto-check for updates using an interval",
        default=False,
    )

    updater_intrval_months: IntProperty(
        name='Months',
        description="Number of months between checking for updates",
        default=0,
        min=0
    )
    updater_intrval_days: IntProperty(
        name='Days',
        description="Number of days between checking for updates",
        default=7,
        min=0,
        max=31
    )
    updater_intrval_hours: IntProperty(
        name='Hours',
        description="Number of hours between checking for updates",
        default=0,
        min=0,
        max=23
    )
    updater_intrval_minutes: IntProperty(
        name='Minutes',
        description="Number of minutes between checking for updates",
        default=0,
        min=0,
        max=59
    )

    # settings

    domain: StringProperty(name="FTP Domain")
    login: StringProperty(name="FTP Login")
    password: StringProperty(name="FTP Password", subtype='PASSWORD')
    ftp_folder: StringProperty(name="FTP Project Folder", default='/')
    local_folder: StringProperty(
        name="Local Project Folder", subtype='DIR_PATH')
    passive_connection: BoolProperty(name="Passive Connexion", default=True)
    admin_mode: BoolProperty(name="Admin", default=False,
                             description='More complex sync filter, menu options to run batch sync')
    sync_type: EnumProperty(
        name="Sync Type",
        # items = [(i,i.title(),"") for i in ("ONLY_NEW", "OVERWRITE", "MORE_RECENT")],
        items=(
            ('ONLY_NEW', 'Only new',
             'Only new file are pushed, skip laready existing files'),
            ('MORE_RECENT', 'More recent',
             'Overwrite file only if source is more recent'),
            ('OVERWRITE', 'Overwrite',
             'Always Overwrite file even if exists (even if source is older)'),
        ),
        default="ONLY_NEW")

    last_version: BoolProperty(name="Only last version", default=True,
                               description='When folder contain versionned files, it will sync last of the files with same name')
    include_filter: CollectionProperty(type=IncludePath)

    exclude_filter: CollectionProperty(type=ExclusionPatterns)

    def draw(self, context):
        # prefs = context.preferences.addons[__package__].preferences
        addon_folder = dirname(__file__)
        layout = self.layout

        row = layout.row(align=True)
        row.prop(self, "pref_tabs", expand=True)

        layout = self.layout
        if self.pref_tabs == 'PREF':
            layout.prop(self, "domain")
            layout.prop(self, "login")
            layout.prop(self, "password")
            layout.prop(self, "ftp_folder")
            layout.prop(self, "passive_connection")
            layout.prop(self, "local_folder")

            layout.separator()


            layout.prop(self, "last_version")
            # sync type (if admin)
            row = layout.row()
            row.prop(self, "admin_mode")
            if self.admin_mode:  # sync type only accessible through admin mode
                row.label(text="Sync type")
                row.prop(self, "sync_type", expand=True)

            # crendential manager line
            row = layout.row(align=True)
            row.label(text="Credentials")
            row.operator("ftpsync.load_credentials", text='Load settings',
                         icon='IMPORT').filepath = addon_folder.rstrip('\\/') + '/'

            row.operator("ftpsync.save_credentials", text='Save settings',
                         icon='EXPORT').filepath = addon_folder.rstrip('\\/') + '/ftp_settings.json'

            row.operator("wm.path_open", text='Open settings location',
                         icon='FOLDER_REDIRECT').filepath = addon_folder

            # Filters
            # Only include those Path
            box = layout.box()
            row = box.row(align=True)
            # row.operator("ftpsync.add_filter_folder", icon="ADD", text="")
            row.operator("ftpsync.add_filter_folder", icon="ADD",
                         text="").name = 'include_filter'
            row.label(text='Only sync those sub-path (if nothing specified: all ftp root project)')

            for i, collec_item in enumerate(self.include_filter):
                row = box.row(align=True)
                row.prop(collec_item, "path", text='')
                op = row.operator("ftpsync.remove_filter_folder",
                                  icon="REMOVE", text="")
                op.name = 'include_filter'
                op.index = i

            # Exclude pattern

            box = layout.box()
            row = box.row(align=True)
            # row.operator("ftpsync.add_filter_folder", icon="ADD", text="")
            row.operator("ftpsync.add_filter_folder", icon="ADD",
                         text="").name = 'exclude_filter'
            row.label(text='Exclude pattern (fnmatch pattern, e.g: *.png exclude all png file. concern folder too)')

            for i, collec_item in enumerate(self.exclude_filter):
                row = box.row(align=True)
                row.prop(collec_item, "pattern", text='')
                op = row.operator("ftpsync.remove_filter_folder",
                                  icon="REMOVE", text="")
                op.name = 'exclude_filter'
                op.index = i

        if self.pref_tabs == 'UPDATE':
            addon_updater_ops.update_settings_ui(self, context)  # upm

# --- /credentials manager


class BFTP_OT_load_credentials(Operator, ImportHelper):
    bl_idname = "ftpsync.load_credentials"
    bl_label = "Load ftp credentials"
    # bl_options = {'REGISTER', 'INTERNAL'}

    filename_ext = '.json'

    # *.jpg;*.jpeg;*.png;*.tif;*.tiff;*.bmp
    filter_glob: bpy.props.StringProperty(default='*.json', options={'HIDDEN'})

    filepath: bpy.props.StringProperty(
        name="File Path",
        description="File path used for import",
        maxlen=1024)

    def execute(self, context):
        # set pref variable from json content
        prefs = get_addon_prefs()

        settings_dic = None
        with open(self.filepath, 'r') as fd:
            settings_dic = json.load(fd)

        ## TODO kill filters list (?) currently weirdly additive...

        for k, v in settings_dic.items():
            if k == 'include_filter':
                cur_include_filter = [i.path for i in prefs.include_filter]
                for path in v:
                    if path in cur_include_filter:
                        continue
                    prefs.include_filter.add()
                    prefs.include_filter[-1].path = path


            elif k == 'exclude_filter':
                cur_exclude_filter = [i.pattern for i in prefs.exclude_filter]
                for pat in v:
                    if pat in cur_exclude_filter:
                        continue
                    prefs.exclude_filter.add()
                    prefs.exclude_filter[-1].pattern = pat
                
            else:
                setattr(prefs, k, v)

        self.report(
            {'INFO'}, f'settings loaded from: {basename(self.filepath)}')

        return {'FINISHED'}


class BFTP_OT_save_credentials(Operator, ExportHelper):
    bl_idname = "ftpsync.save_credentials"
    bl_label = "Save ftp credentials"
    # bl_options = {'REGISTER', 'INTERNAL'}

    # *.jpg;*.jpeg;*.png;*.tif;*.tiff;*.bmp
    filter_glob: bpy.props.StringProperty(default='*.json', options={'HIDDEN'})

    filename_ext = '.json'

    filepath: bpy.props.StringProperty(
        name="File Path",
        description="File path used for export",
        maxlen=1024)

    def execute(self, context):
        # dump pref variable in json
        prefs = get_addon_prefs()
        print(f'Saving ftp settings: {self.filepath}')

        settings_dic = {}
        # "sync_type"
        for attr in ("domain", "login", "password", "ftp_folder", "local_folder", "passive_connection",):
            settings_dic[attr] = getattr(prefs, attr)
        
        include_filter = [i.path for i in prefs.include_filter]
        if include_filter:
            settings_dic['include_filter'] = include_filter

        exclude_filter = [i.pattern for i in prefs.exclude_filter]
        if exclude_filter:
            settings_dic['exclude_filter'] = exclude_filter

        with open(self.filepath, 'w') as fd:
            json.dump(settings_dic, fd, indent='\t')

        self.report({'INFO'}, f'saved: {self.filepath}')  # WARNING, ERROR
        return {'FINISHED'}


cls = (
    BFTP_OT_load_credentials,
    BFTP_OT_save_credentials,  # first (operator displayed in Preferences)
    IncludePath,
    ExclusionPatterns,
    PIPE_OT_add_filter_folder,
    PIPE_OT_remove_filter_folder,

    PIPE_Preferences,
)


def register():
    for c in cls:
        bpy.utils.register_class(c)


def unregister():
    for c in cls:
        bpy.utils.unregister_class(c)
