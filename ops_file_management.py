### file management overview
# - basic save increment
# - save next task (according to task in project template) 
# - open shot by typing shot id (hadrcoded) ex: construct 's02p01' from input '02 01' and find shot folder from root


import bpy
import re
from pathlib import Path
from os.path import basename
from bpy.types import Operator

from . import template
# from .template import Template
from . path_func import get_addon_prefs, check_root_path, open_folder, clean_path

#tpl_shot = template.Template('{root}/shots/S{sequence:02d}_P{shot:02d}/{task}/{task}_s{sequence:02d}_p{shot:02d}_v{version:03d}.blend')

template_loc = str(Path(__file__).parent / 'def_template.yml')
print('template_loc: ', template_loc)

# load template file into a template object
tpl = template.Templates(template_loc)


class PIPE_OT_save_increment(Operator):
    '''Save current state in a new file with next version number'''
    bl_idname = "pipe.save_incremental"
    bl_label = "Incremental save"
    bl_options = {'REGISTER'}

    def execute(self, context):
        # prefs = get_addon_prefs()
        # if not prefs.local_folder:
        #     return {'CANCELLED'}
        print('------ save and increment')
        """ data = #.parse(bpy.data.filepath)
        increment_path = #.format(data, version=template.LAST_NEXT) """

        if not re.search(r'v\d{3}\.\w+$', basename(bpy.data.filepath)):
            mess = 'no version to increment "v???.ext" found in filename'
            self.report({'ERROR'}, mess)
            return {'CANCELLED'}

        try:
            data = tpl['shot'].parse(bpy.data.filepath)
        except Exception as e:
            mess = 'check console, Cannot parse filepath with template, check the name and position of the file in folder hierarchy.'
            self.report({'ERROR'}, mess)
            print(e)
            return {'CANCELLED'}

        if not data:
            self.report({'ERROR'}, f'File is out of pipe or naming is wrong. (Parsing filepath return None)')
            return {'CANCELLED'}
        
        print('shot data: ', data)

        increment_path = tpl['shot'].format(data, version=template.NEXT)
        print('increment: ', increment_path)
        
        new_blend = Path(increment_path)

        #test if exists
        if new_blend.exists():
            mess = f'Next version already exists ! ({new_blend.name})'
            self.report({'ERROR'}, mess)
            return {'CANCELLED'}
        
        #save the current blend at new adress.
        # check_existing (boolean, (optional)) – Check Existing, Check and warn on overwriting existing files
        bpy.ops.wm.save_as_mainfile(
            filepath=str(new_blend), check_existing=False, 
            compress=False, relative_remap=False, copy=False)
        
        self.report({'INFO'}, f'Increment saved as {new_blend.name}')
        return {'FINISHED'}


class PIPE_OT_save_next_task(Operator):
    '''Save as new file in the next task (layout > anim > colo) reseting version counter.'''
    bl_idname = "pipe.save_next_task"
    bl_label = "Save to next task"
    bl_options = {'REGISTER'}

    def execute(self, context):
        # prefs = get_addon_prefs()
        # if not prefs.local_folder:
        #     return {'CANCELLED'}
        print('------ next task')
        blend = Path(bpy.data.filepath)
        if not re.search(r'v\d{3}\.\w+$', blend.name):
            mess = 'problem with file naming, no version "v???.ext" found'
            self.report({'ERROR'}, mess)
            return {'CANCELLED'}

        try:
            data = tpl['shot'].parse(str(blend))
        except Exception as e:
            mess = 'check console, Cannot parse filepath with template.\ncheck the name and position of the file in folder hierarchy.\nMight be out of pipe.'
            self.report({'ERROR'}, mess)
            print(e)
            return {'CANCELLED'}

        if not data:
            self.report({'ERROR'}, f'File is out of pipe or naming is wrong. (Parsing filepath return None)')
            return {'CANCELLED'}

        print('shot data: ', data)
        
        # reset version to 000 or 001 and
        new_path = tpl['shot'].format(data, task=template.NEXT, version=0)
        print('next task: ', new_path)
        

        new_blend = Path(new_path)

        #test if exists
        if new_blend.exists():
            mess = f'Next version already exists ! ({new_blend.name})'
            self.report({'ERROR'}, mess)
            return {'CANCELLED'}

        # create folder path if not exists
        new_blend.parent.mkdir(parents=True, exist_ok=True)

        #save the current blend at new adress.
        bpy.ops.wm.save_as_mainfile(
            filepath=str(new_blend), check_existing=False, 
            compress=False, relative_remap=False, copy=False)
        
        info = bpy.data.texts.get('info')
        if not info:
            info = bpy.data.texts.new('info')
            print ("infos text block created")
        
        #go to beginning
        
        from time import strftime
        date = strftime("%Y-%m-%d %H:%M:%S")
        
        info.current_line_index = 0
        info.write(f'Created from {blend.name} at {date}'+'\n')
        # info.lines[0].body = f'Created from {blend.name}' + info.lines[0].body

        self.report({'INFO'}, f'Increment saved as {new_blend.name}')
        return {'FINISHED'}

# Quick folder opener


class PIPE_OT_open_shot_folder(Operator):
    '''Open shot folder from current blender'''
    bl_idname = "pipe.open_shot_folder"
    bl_label = "Open chosen shot folder"
    bl_options = {'REGISTER'}

    mysid : bpy.props.StringProperty(name="shot id", default="", options={'SKIP_SAVE'})

    def invoke(self, context, event):
        prefs = get_addon_prefs()
        if not prefs.local_folder:
            self.report({'ERROR'}, f'Project local folder is not specified in addon preferences')
            return {'CANCELLED'}

        ## root path error handling
        self.root = clean_path(prefs.local_folder)
        err = check_root_path(self.root)
        if err:
            self.report({'ERROR'}, err)
            return {'CANCELLED'}

        # # show box for shot id:
        # ## search for 's01_p01' ou 's01p01' in the filename
        # sid = re.search(r's(\d{2})_?p(\d{2}).*.blend$', basename(bpy.data.filepath), re.IGNORECASE)
        # if sid:
        #     self.mysid = f"{sid.group(1)}_{sid.group(2)}"
        #     return self.execute(context)# if sid found in filename run execute directly

        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout
        layout.label(text='Quel est le shot ID de ce plan ? (ex: "s02p01", autre ex pour ouvrir anim "a0201"')
        layout.prop(self, 'mysid')#, text="")#, text=""

    def execute(self, context):
        ## loose search of shotid with two firsts two digits numbers.
        sid = re.search(r'(\d{2}).*?(\d{2})', self.mysid, re.IGNORECASE)
        if not sid:
            self.report({'ERROR'}, 'No correct shot ID found')
            return {'CANCELLED'}
        
        shotfolder = f"S{sid.group(1)}_P{sid.group(2)}"
        # fullshotid = f"s{sid.group(1)}_p{sid.group(2)}"
        
        # construct filepath
        shotpath = Path(self.root) / 'shots' / shotfolder
        # filename = f'layout_{fullshotid}_v001.blend'

        if self.mysid.startswith('l'):
            shotpath = shotpath / 'layout'
        elif self.mysid.startswith('anima'):
            shotpath = shotpath / 'animatic'
        elif self.mysid.startswith('a'):
            shotpath = shotpath / 'anim'
        elif self.mysid.startswith('col'):
            shotpath = shotpath / 'colo'
        elif self.mysid.startswith('com'):
            shotpath = shotpath / 'compo'

        if not shotpath.exists():
            self.report({'ERROR'}, f'Following path not exists : {str(shotpath)}')
            if not shotpath.parent.exists():
                return {'CANCELLED'}
            # use parent path
            shotpath = shotpath.parent

        open_folder(str(shotpath))
        
        self.report({'INFO'}, f'Opened {str(shotpath)}')
        return {'FINISHED'}

# quick folder opener


cls = (
PIPE_OT_save_increment,
PIPE_OT_save_next_task,
PIPE_OT_open_shot_folder,
)

def register():
    for c in cls :
        bpy.utils.register_class(c)

def unregister():
    for c in cls :
        bpy.utils.unregister_class(c)