import os
from os.path import *
from . path_func import get_addon_prefs
import re
import itertools

def connect_to_ftp(domain, login, passwd, passive = True) :
    from ftplib import FTP
    ftp = FTP(domain,timeout = 10)
    ftp.login(user = login, passwd = passwd)
    ftp.set_pasv(passive)

    return ftp

def check_credentials(self, context):
    prefs = get_addon_prefs()
    mess = None
    if not prefs.domain or not prefs.login or not prefs.password:
        mess = 'Some FTP credendials are missing in preferences'
    
    if not prefs.local_folder:
        mess = 'Local root folder is not specified in preferences'

    if not exists(prefs.local_folder):
        mess = 'Local root folder not found'

    if mess:
        self.report({'ERROR'}, mess)#WARNING, ERROR
    return mess

def is_exclude(name, patterns) :
    from fnmatch import fnmatch

    if not isinstance(patterns, (list,tuple)) :
        patterns = [patterns]

    return any([fnmatch(name, p) for p in patterns])


def get_ftp_files(ftp, root, exclusions) :
    '''
    Recursively get all filepath on ftp at given url
    ftp: connected ftp object
    root: url in ftp where to retrieve files
    exclusions: a list of fnmatch compatible pattern to exclude files
    Return a list of all filepath in passed folder
    '''
    files = []

    ftp.cwd(root)
    for k,v in ftp.mlsd() :
        ## k = filename and v is an object 
        if is_exclude(k, exclusions) : continue

        path = '/'.join([root,k])
        if v['type'] == 'file' :
            files.append(path)

        elif v['type'] == 'dir' :
            files+= get_ftp_files(ftp, path, exclusions)

    return sorted(files)


def get_last_ftp_files(ftp, root, exclusions) :
    '''
    Recursively get all filepath on ftp at given url
    ftp: connected ftp object
    root: url in ftp where to retrieve files
    exclusions: a list of fnmatch compatible pattern to exclude files
    Return a list of all filepath in passed folder
    '''

    pattern = r'_v\d{3}\.'

    files = []

    cur_files = []
    cur_dirs = []

    ftp.cwd(root)
    for k,v in ftp.mlsd():
        ## k = filename and v is an object 
        if is_exclude(k, exclusions) : continue

        path = '/'.join([root,k])
        if v['type'] == 'file' :
            cur_files.append(path)

        elif v['type'] == 'dir' :
            cur_dirs.append(path)
    
    ## sort files to get last
    cur_files 
    for i in range(len(cur_files)-1,-1,-1):# fastest way to iterate on index in reverse
        if not re.search(pattern, basename(cur_files[i])):
            # directly append file that don't have version number
            files.append(cur_files.pop(i))
    
    # separate remaining files in a list of grouped sublists by name
    lilist = [list(v) for k, v in itertools.groupby(cur_files, key=lambda x: re.split(pattern, basename(x))[0])]
    
    # Get only last item of each sorted grouplist
    for l in lilist:
        ## print chosen one and skipped list 
        # print(f'Take {basename(sorted(l)[-1])} out of {len(l)} versions ({[basename(x) for x in sorted(l)[:-1]]})')
        if len(l) > 1:
            print(f'Taken {basename(sorted(l)[-1])} out of {len(l)} versions')
        files.append(sorted(l)[-1])

    ## now explore dirs in current dis 
    for path in cur_dirs:
        files+= get_last_ftp_files(ftp, path, exclusions)

    return sorted(files)

def get_files(root, exclusions) :
    '''Recursively get files in passed directory if not in exclusion list'''

    files = []
    for f in os.scandir(root) :
        if is_exclude(f.path, exclusions) : continue

        if f.is_file() :
            files.append(f.path)

        elif f.is_dir() :
            files+= get_files(f.path, exclusions)

    return sorted(files)

def get_last_files(root, exclusions) :
    '''
    same as 'get_files' except when multiple files has same name with version,
    keep only the latest 
    Recursively get files in passed directory if not in exclusion list
    Also, if some file have versionning in name, take only the last one
    '''
    
    files = []
    all_items = [f for f in os.scandir(root) if not is_exclude(f.path, exclusions)]
    allfiles = [f for f in all_items if f.is_file()]
    dirs = [f for f in all_items if f.is_dir()]

    for i in range(len(allfiles)-1,-1,-1):# fastest way to iterate on index in reverse
        if not re.search(r'_v\d{3}\.\w+', allfiles[i].name):
            # print(i, allfiles[i].name)#Dbg
            files.append(allfiles.pop(i).path)#pop and 
        
    # separate remaining files in prefix grouped lists
    lilist = [list(v) for k, v in itertools.groupby(allfiles, key=lambda x: x.name.split('_')[0])]
    
    # get only item last of each sorted grouplist
    for l in lilist:
        files.append( sorted(l, key=lambda x: x.name)[-1].path )

    for d in dirs:#recursively treat all detected directory
        files += get_last_files(d.path, exclusions)

    return sorted(files)


def createDirs(ftp, dirpath):
    from ftplib import error_perm

    """
    Create dir with subdirs (progressive dir creation).

    :param ftp:     connected FTP
    :param dirpath: path (like 'test/test1/test2')

    :type ftp:      FTP
    :type dirpath:  str
    :rtype:         None

    """

    dirpath = dirpath.replace('\\', '/')
    tmp = dirpath.split('/')
    dirs = []

    # List of all component in path with fullpath to it
    for part in tmp:
        if len(dirs) == 0:
            dirs.append(part)
            continue

        dirs.append(dirs[-1] + '/' + part)

    # Try to create dirpath progressively
    for dfp in dirs:
        try:
            ftp.mkd(dfp)
            print(f'created directories on ftp : {dfp}')
        except error_perm as e:
            e_str = str(e)
            if '550' in e_str and 'File exists' in e_str:
                continue


def exist_ftp_file(ftp, filepath) -> (bool, str) :
    '''
    If file exists return modification time in seconds
    True if modification time could not be retrieved
    else return False
    '''
    filepath = filepath.replace('\\', '/')
    #ftp.cwd('/')

    if filepath.startswith('/') : filepath = filepath[1:]

    split_path = filepath.split('/')

    for i, path in enumerate(split_path) :
        #print('  -', path)#Dbg print each parts of path as it crawl
        list_dir = {k:v['type'] for k,v in ftp.mlsd()}
        #print(i,path, list(list_dir.keys()))
        if path in list_dir.keys() :
            if i == len(split_path)-1 :
                # Exists, found it return true or modif time if exists
                
                list_date = {k:v['modify'] for k,v in ftp.mlsd()}
                modif = list_date.get(path)
                if modif:
                    return modif
                ## direct cmd to get time, return '213 20200616221913' (213 must mean success)
                # mtime = ftp.sendcmd(f'MDTM {path}')

                return True

            elif list_dir[path] == 'dir' :
                ftp.cwd(path)
            else :
                return False
        else :
            return False