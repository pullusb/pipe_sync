import bpy, re, os
from os.path import basename, exists, splitext
from sys import platform
import subprocess

from typing import Optional#Union

# group match: task, sequence, shot, version
re_file = re.compile(r'(?P<task>[a-zA-Z]+)_s(?P<sequence>\d{2})_p(?P<shot>\d{2})_v(?P<version>\d{3})\.blend$', re.IGNORECASE)
re_shotid = re.compile(r'(s\d{2}_p\d{2}).*.blend$', re.IGNORECASE)


def clean_path(path):
    '''Return a posix path ending with a slash'''
    clean_path = path.replace('\\','/').rstrip('/')
    # '/' if root stripped, so add it here if needed (and handle Case of project at root of a drive )
    if clean_path.endswith(':') or len(clean_path) < 2:
        clean_path += '/'
    return clean_path

def get_addon_prefs():
    '''
    function to read current addon preferences properties

    access a prop like this :
    prefs = get_addon_prefs()
    option_state = prefs.super_special_option

    oneliner : get_addon_prefs().super_special_option
    '''
    import os
    addon_name = os.path.splitext(__name__)[0]
    preferences = bpy.context.preferences
    addon_prefs = preferences.addons[addon_name].preferences
    return (addon_prefs)

def check_valid_filename():
    match_file = re_file.search(basename(bpy.data.filepath))
    if not match_file:
        return
    return match_file

def get_shot_id():
    '''Return shot id (S01_P02) in upper case (detect case insensitive)'''
    match_sid = re_shotid.search(basename(bpy.data.filepath))
    if not match_sid:
        return
    return match_sid.group(1).upper()


def create_dir(dirpath):
    '''Create dir at path if not exists'''
    if not exists(dirpath):
        os.mkdir(dirpath)
    return dirpath

def open_folder(folderpath):
    """open the folder at the path given with cmd relative to user's OS"""
    myOS = platform
    if myOS.startswith('linux') or myOS.startswith('freebsd'):
        cmd = 'xdg-open'
    elif myOS.startswith('win'):
        cmd = 'explorer'
        if not folderpath:
            return('/')  
    else:
        cmd = 'open'
    if not folderpath:
        return('//')

    # to prevent bad path string use normpath: 
    folderpath = os.path.normpath(folderpath)
    fullcmd = [cmd, folderpath]
    subprocess.Popen(fullcmd)
    return ' '.join(fullcmd)#back to string to return and print


def check_root_path(root) -> Optional[str]:# Union[str, None]
    '''
    root path error handling
    return nothing if OK, error message if problem
    '''
    if not root or not exists(root):
        return 'project root path not exists\nput the right path in addon preferences'
    
    if not 'shots' in os.listdir(root):
        return 'specified project root path seems incorrect (could not found containing "shots" folder)'


def check_texts_editors(name='info'):
    '''
    check text editors in UI for specific text name
    if found with wnanted text - return 'ok' (True)
    if text editor in UI but not wanted text - set wanted text in first tex editor found
    if no text editor or no text data - return False
    '''
    
    #check if text exists
    if not bpy.data.texts.get(name):
        print("text block '{}' not exists".format(name))
        return
    
    ct = 0
    for window in bpy.context.window_manager.windows:
        screen = window.screen
        for area in screen.areas:
            if area.type == 'TEXT_EDITOR':
                ct += 1
                for space in area.spaces:
                    if space.type == 'TEXT_EDITOR':
                        if space.text.name == name:
                            print("Text '{}' found in UI".format(name))#Dbg
                            return ('ok')

        #change text in zone if found
        if ct:
            print(ct, 'text editor found')
            for area in screen.areas:
                if area.type == 'TEXT_EDITOR':
                    for space in area.spaces:
                        if space.type == 'TEXT_EDITOR':
                            
                            space.text = bpy.data.texts[name]
                            print("Text '{}' placed in UI".format(name))#Dbg
                            return ('ok')
    
    #no text UI found:
    print('no text editor UI found')#Dbg
    return False


def is_video(fp) -> bool:
    '''Return True if is repertoried video format else False, None if no extension detected'''

    videoext = ('mkv', 'mov', 'mp4', 'avi', 'wmv', 'webm', 
    'mpg', 'gif', 'flv', 'mpeg', 'MTS', 'M2TS', 'TS', 
    'm2v', 'm4a', 'm4v', 'f4v', 'f4a', 'm4b', 'm4r', 
    'f4b', '3gp', '3gp2', '3g2', '3gpp', '3gpp2')
    ext = splitext(fp)[1]
    if not ext:
        print(fp, 'has no extension')
        return

    return ext.lower()[1:] in videoext

""" 
def is_video(fp):
    # check if videotype using mimetype (seems slow)
    import mimetypes
    if mimetypes.guess_type(fp)[0].startswith('video'):
        return True
 """