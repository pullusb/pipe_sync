bl_info = {
    "name": "Pipe sync",
    "author": "Samuel Bernou, Christophe Seux",
    "description": "Pipeline tools with project FTP push/pull",
    "version": (1, 9, 1),
    "blender": (2, 83, 0),
    "warning": "",
    "doc_url": "https://gitlab.com/pullusb/pipe_sync",
    "category": "User",
}

import bpy
import os
from os.path import *
import subprocess

# import local files
from pathlib import Path

#### --------------- 
###  Install module dependancies 
#### ---------------

## module_name, package_name
DEPENDENCIES = {
    ('yaml', 'PyYAML'),
}

from . import environment

error = environment.pip_install_and_import(DEPENDENCIES)
if error:
    raise Exception('Cannot import modules (see console). Try restarting blender as admin') from error
    # try:#hack to print error with error popup when trying to register
    #     1 / 0
    # except ZeroDivisionError as e:
    #     raise Exception('Cannot import modules (see console). Try restarting blender as admin') from e

# try:
#     # environment.setup(DEPENDENCIES)
#     # environment.pip_install('PyYAML')
#     environment.pip_install_and_import(DEPENDENCIES)
# except ModuleNotFoundError:
#     print("Fail to install modules dependencies, try to execute blender with admin rights.")
#     return

from . import addon_updater_ops# upm
from . import prefs
from . import ops_ftp_sync
from . import ops_file_management# template need yaml
from . import ui_menu

#### --------------- 
###     register 
#### ---------------


def register():
    bpy.types.Scene.ftps_dryrun = bpy.props.BoolProperty(name="test mode", description="don't do anything, just print in changes in console", default=False)
    addon_updater_ops.register(bl_info)# upm
    prefs.register()
    ops_ftp_sync.register()
    ops_file_management.register()
    ui_menu.register()

    ## dev tool
    # bpy.context.window_manager.addon_search = bl_info['name']
    bpy.context.preferences.addons[__package__].preferences.admin_mode = True
    # bpy.ops.ftpsync.load_credentials(filepath=r'')

def unregister():
    ui_menu.unregister()
    ops_ftp_sync.unregister()
    ops_file_management.unregister()
    prefs.unregister()
    addon_updater_ops.unregister()# upm

    del bpy.types.Scene.ftps_dryrun



# if __name__ == "__main__":
#     register()