import bpy
from .path_func import get_addon_prefs
from os.path import dirname
# from . import addon_updater_ops# upm
class PIPE_MT_manager_menu(bpy.types.Menu):
    bl_label = "Manager"
    bl_idname = "PIPE_MT_manager_menu"

    def draw(self, context):
        layout = self.layout
        layout.operator("pipe.save_incremental", icon='PLUS')
        layout.operator("pipe.save_next_task", icon='SEQ_STRIP_DUPLICATE')#RIGHTARROW
        layout.operator("wm.path_open", text='Open this blend folder', icon='FILE_FOLDER').filepath = dirname(bpy.data.filepath)
        layout.operator("pipe.open_shot_folder", icon='FOLDER_REDIRECT')
        layout.separator()
        layout.operator("ftpsync.send_current", icon='EXPORT')
        layout.operator("ftpsync.get_shot", icon='IMPORT')
        layout.operator("ftpsync.get_dependencies", icon='IMPORT')
        
        prefs = get_addon_prefs()
        if prefs.admin_mode:
            # layout.separator()
            layout.menu('PIPE_MT_all_sync_menu')
            layout.prop(prefs, "sync_type", text='')
            ## sync mode choice from menu
        layout.prop(context.scene, "ftps_dryrun")
        
        ## update entry
        # addon_updater_ops.update_notice_box_ui()# upm


class PIPE_MT_all_sync_menu(bpy.types.Menu):
    '''Push/pull all files between local/remote(ftp) project, according to "Sync mode" rule'''
    bl_label = "FTP sync all"
    bl_idname = "PIPE_MT_all_sync_menu"

    def draw(self, context):
        layout = self.layout
        layout.operator("ftpsync.send", icon='EXPORT', text='Send all last versions to FTP').last_versions = True
        layout.separator()
        layout.operator("ftpsync.send", icon='EXPORT', text='Send all to FTP').last_versions = False
        layout.operator("ftpsync.get", icon='IMPORT')


def menu_draw(self,context):
    self.layout.menu("PIPE_MT_manager_menu")

def register():
    bpy.utils.register_class(PIPE_MT_all_sync_menu)
    bpy.utils.register_class(PIPE_MT_manager_menu)
    bpy.types.TOPBAR_MT_editor_menus.append(menu_draw)

def unregister():
    bpy.types.TOPBAR_MT_editor_menus.remove(menu_draw)
    bpy.utils.unregister_class(PIPE_MT_manager_menu)
    bpy.utils.unregister_class(PIPE_MT_all_sync_menu)